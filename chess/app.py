from fastapi import FastAPI

app = FastAPI()


# Endpoint to start a new game
@app.get("/new_game")
def create_new_game():
    """
    New game creation function
    """
    # Logic for creating a new game should be here
    # For example, creating a new chessboard object and identifying it
    game_id = 1  # Just an example, replace with your logic
    return {"game_id": game_id}


# Endpoint to get the game state
@app.get("/game_state/{game_id}")
def get_game_state(game_id: int):
    """
    Get game result function
    """
    # Logic for getting the game state based on game_id should be here
    # For example, piece positions, current turn, etc.
    return {"game_id": game_id, "state": "in_progress"}  # Just an example


# Endpoint to make a move
@app.post("/make_move/{game_id}")
def make_move(game_id: int, move: str):
    """
    Make move changing url
    """
    # Logic for processing a move should be here
    # For example, updating the game state after a move
    return {"game_id": game_id, "move": move}  # Just an example
